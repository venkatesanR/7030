mvn archetype:generate -DgroupId=com.acube -DartifactId=7030_APP -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false


==========================================================================================
GIT code checkin procedures
==========================================================================================
Git Repositary Statrup guide
------------------------
echo # wp_spring_intg >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/venkatesanR/wp_spring_intg.git
git push -u origin master
git init is required start repositary
-------------------------------------
>git init

git clone
--------
>git clone /path/to/repository(local path)
>git clone username@host:/path/to/repository

git add and commit
----------------
>git add <filename>
>git add *(This is the first step in the basic git workflow. To actually commit these changes use)

>git commit -m "Commit message"(Now the file is committed to the HEAD, but not in your remote repository yet.)

git push changes
----------------
Your changes are now in the HEAD of your local working copy. To send those changes to your remote repository, execute

>git push origin master

If you have not cloned an existing repository and want to connect your repository to a remote server, you need to add it with
 > git remote add origin <server>
Now you are able to push your changes to the selected remote server

update and merge
to update your local repository to the newest commit, execute 
 >git pull
in your working directory to fetch and merge remote changes.
to merge another branch into your active branch (e.g. master), use
git merge <branch>
in both cases git tries to auto-merge changes. Unfortunately, this is not always possible and results in conflicts. You are responsible to merge those conflicts manually by editing the files shown by git. After changing, you need to mark them as merged with
 > git add <filename>
before merging changes, you can also preview them by using
 > git diff <source_branch> <target_branch>
 
============================================================================================================
  NODE AND ITS DEPENDENCY MANAGEMENT
============================================================================================================
###############################################################################################
#Prerequisite							   #
###############################################################################################
   http://nodejs.org/
   Note : By default npm folder will get create under C:\Users\thiyagu.selvaraj\AppData\Roaming\npm
   NPM - node package manager
   Add "C:\<UserFolder>\apps\nodejs" folder to the windows path.

   Current Version: v0.10.30
###############################################################################################
1) Install Bower from command prompt
   
npm install -g grunt-cli bower 
   
   Note : Now you can see "bower" and "grunt-cli" folder under C:\<UserFolder>/AppData\Roaming\npm\node_modules

###############################################################################################
2) Goto C:\projects\<ur war folder> folder 

bower install

###############################################################################################
3) Install project dependencies with npm install
   Goto C:\projects\<ur war folder> 

npm install

###############################################################################################
4) Grunt: The JavaScript Task Runner
   Goto C:\projects\<ur war folder> folder 

grunt 
======================================================================================================