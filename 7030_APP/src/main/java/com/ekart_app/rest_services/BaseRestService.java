package com.ekart_app.rest_services;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;

public class BaseRestService extends ResourceConfig {
	public BaseRestService() {
		register(RequestContextFilter.class);
		packages("com.ekart_app.rest_services");
		register(JacksonFeature.class);
		register(RequestHandlers.class);
	}
}
