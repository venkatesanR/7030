package com.ekart_app.rest_services;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ekart_app.ui_models.Login;
import com.ekart_app.ui_utils.UtilService;
@Component
@Path("/login_service")
public class LoginRS {
	private static final Logger logger = Logger.getLogger(LoginRS.class);

	@Autowired
	@Qualifier("uiService")
	private UtilService utility;

	@POST
	@Path("/sign_in/{create_user}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Login getHomeDetails(@PathParam("create_user") Boolean isLogin,Login homeDetails) {
		return null;
	}
}