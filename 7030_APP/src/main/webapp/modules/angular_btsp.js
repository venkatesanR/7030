(function() {
    'use strict';
    var angular_btap = angular.module('angular_btap', ['ngRoute', 'ngResource']);
    angular_btap.value('serviceapi', apiurl);
    angular_btap.config(['$routeProvider', function($routeProvider) {
        $routeProvider.
        when('/', {
            controller: 'LoginController',
            templateUrl: 'views/login.html'
        }).
        when('/home', {
            controller: 'LoginController',
            templateUrl: 'views/login.html'
        });
    }]);

})(); // ends main function
