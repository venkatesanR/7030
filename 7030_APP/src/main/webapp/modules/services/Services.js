   (function() {
       'use strict';
       angular.module('angular_btap').factory('AppServices', ['$resource', 'serviceapi',
           function($resource, serviceapi) {
               return $resource('', {}, {
                   sign_in_or_up: {
                       method: 'POST',
                       url: serviceapi.POST_SIGN_IN_OR_UP,
                       timeout: 100000
                   }
               });
           }
       ]);
   })(); // ends main function
